# CDA (Convenient Door Authentication)

## Funktionen
App: Request (Tür öffnen/schließen), Stream sehen, NFC

Website: Request (Tür öffnen/schließen), Stream sehen, Model/Bilder verwalten

Server: Zentraler Punkt

Raspberry: Tür öffnen/schließen, NFC, Feedback (z.B Sound)

## Team
Duy Bui bui16910@spengergasse.at

Matthew Calma cal16913@spengergasse.at

Florian Feka florianfeka05@gmail.com

Kyrillus Mehanni meh17008@spengergasse.at

Stefan Tirea tir17074@spengergasse.at

