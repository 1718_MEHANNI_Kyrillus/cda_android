package com.example.kyrill.cda;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.support.v4.content.ContextCompat;
import android.test.ActivityInstrumentationTestCase2;
import android.support.v7.widget.AppCompatImageButton;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.example.kyrill.cda.ServerUtils.BUTTON_STATE_CLOSE;
import static com.example.kyrill.cda.ServerUtils.BUTTON_STATE_OPEN;


@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest extends ActivityInstrumentationTestCase2<HomeActivity> {
    private HomeActivity homeActivity;

    public ExampleInstrumentedTest() {
        super(HomeActivity.class);
    }

    @Before
    public void setup() throws Exception {
        super.setUp();

        setActivityInitialTouchMode(false);
        injectInstrumentation(InstrumentationRegistry.getInstrumentation());
        homeActivity = getActivity();
    }

    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.example.kyrill.cda", appContext.getPackageName());
    }

    @Test
    public void testSharedPreferencesDoorState(){
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        // Check Default Door State
        Integer defaultDoorState = UserSession.getDoorState(appContext);
        Assert.assertEquals(BUTTON_STATE_OPEN, defaultDoorState);

        // Set Door State to CLOSE
        UserSession.setDoorState(appContext, BUTTON_STATE_CLOSE);

        // Check if Door State was set correctly
        Integer doorStateClose = UserSession.getDoorState(appContext);
        Assert.assertEquals(BUTTON_STATE_CLOSE, doorStateClose);

        // Set Door State to OPEN
        UserSession.setDoorState(appContext, BUTTON_STATE_OPEN);

        // Check if Door State was set correctly
        Integer doorStateOpen = UserSession.getDoorState(appContext);
        Assert.assertEquals(BUTTON_STATE_OPEN, doorStateOpen);
    }

    /**
     * Tests if the button changes its background on click
     */
    @Test
    public void testButtonDrawableChanged(){
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        final AppCompatImageButton closeDoor = homeActivity.findViewById(R.id.closeDoor);
        final AppCompatImageButton openDoor = homeActivity.findViewById(R.id.openDoor);

        try {
            runTestOnUiThread(closeDoor::performClick);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }


        Drawable.ConstantState actualBackgroundCloseBtn;
        Drawable.ConstantState expectedBackgroundCloseBtn;

        // Close Door Button should have the "pressed" background
        expectedBackgroundCloseBtn = ContextCompat.getDrawable(appContext, R.drawable.circle_black_border_pressed).getConstantState();
        actualBackgroundCloseBtn= closeDoor.getBackground().getConstantState();

        Assert.assertEquals(expectedBackgroundCloseBtn, actualBackgroundCloseBtn);


        Drawable.ConstantState expectedBackgroundOpenBtn;
        Drawable.ConstantState actualBackgroundOpenBtn;
        // Open Door Button should have the "not-pressed" background
        expectedBackgroundOpenBtn = ContextCompat.getDrawable(appContext, R.drawable.circle_black_border).getConstantState();

        actualBackgroundOpenBtn = openDoor.getBackground().getConstantState();
        Assert.assertEquals(expectedBackgroundOpenBtn, actualBackgroundOpenBtn);

        try {
            runTestOnUiThread(openDoor::performClick);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        // Close Door Button should have the "pressed" background
        expectedBackgroundCloseBtn = ContextCompat.getDrawable(appContext, R.drawable.circle_black_border).getConstantState();
        actualBackgroundCloseBtn = closeDoor.getBackground().getConstantState();

        Assert.assertEquals(expectedBackgroundCloseBtn, actualBackgroundCloseBtn);

        // Open Door Button should have the "not-pressed" background
        expectedBackgroundOpenBtn = ContextCompat.getDrawable(appContext, R.drawable.circle_black_border_pressed).getConstantState();
        actualBackgroundOpenBtn = openDoor.getBackground().getConstantState();

        Assert.assertEquals(expectedBackgroundOpenBtn, actualBackgroundOpenBtn);
    }

    /**
     * Tests if the button click changes the door state correctly
     */
    @Test
    public void testButtonChangeDoorState()
    {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        final AppCompatImageButton closeDoor = homeActivity.findViewById(R.id.closeDoor);
        final AppCompatImageButton openDoor = homeActivity.findViewById(R.id.openDoor);

        try {
            runTestOnUiThread(closeDoor::performClick);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        Assert.assertEquals(BUTTON_STATE_CLOSE.intValue(), UserSession.getDoorState(appContext));

        try {
            runTestOnUiThread(openDoor::performClick);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        Assert.assertEquals(BUTTON_STATE_OPEN.intValue(), UserSession.getDoorState(appContext));
    }


}
