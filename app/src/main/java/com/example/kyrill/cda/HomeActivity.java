package com.example.kyrill.cda;

import android.app.DialogFragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Base64;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.neovisionaries.ws.client.WebSocket;
import com.neovisionaries.ws.client.WebSocketAdapter;
import com.neovisionaries.ws.client.WebSocketException;
import com.neovisionaries.ws.client.WebSocketFactory;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

import static com.example.kyrill.cda.ServerUtils.BUTTON_STATE_CLOSE;
import static com.example.kyrill.cda.ServerUtils.BUTTON_STATE_OPEN;
import static com.example.kyrill.cda.ServerUtils.ERROR_MESSAGE;
import static com.example.kyrill.cda.ServerUtils.OK_RESPONSE_CODE;
import static com.example.kyrill.cda.ServerUtils.RESPONSE_CODE;
import static com.example.kyrill.cda.ServerUtils.RESPONSE_MESSAGE;
import static com.example.kyrill.cda.ServerUtils.STREAM_IP;

public class HomeActivity extends AppCompatActivity {

    private static final String LOGGED_USER = "username";
    private Snackbar snackbar;
    private ImageView streamImageView;
    private byte[] decodedString;
    private Bitmap decodedByte;
    TextView userName;
    WebSocket ws = null;
    private static final String CONNECT_MESSAGE = "retrieve";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        scaleButtons();

        userName = findViewById(R.id.userName);

        if (UserSession.getDoorState(this) == BUTTON_STATE_CLOSE) {
            closeDoorPressed();
        } else if (UserSession.getDoorState(this) == BUTTON_STATE_OPEN) {
            openDoorPressed();
        }

        if (UserSession.getUserName(this).length() > 0) {
            userName.setText(UserSession.getUserName(this));
        } else {
            userName.setText(getIntent().getStringExtra(LOGGED_USER));
        }

        streamImageView = findViewById(R.id.stream);

        runNfcCheckInBackground();
    }

    public void closeDoor(View view) {
        //sendRequest(CLOSE_DOOR, userName.getText().toString(), UserSession.getToken(this));
        UserSession.setDoorState(this, BUTTON_STATE_CLOSE);
        closeDoorPressed();
    }

    public void openDoor(View view) {
        //sendRequest(OPEN_DOOR, userName.getText().toString(), UserSession.getToken(this));
        UserSession.setDoorState(this, BUTTON_STATE_OPEN);
        openDoorPressed();
    }

    public void closeDoorPressed() {
        ImageButton closeDoor = findViewById(R.id.closeDoor);
        ImageButton openDoor = findViewById(R.id.openDoor);

        closeDoor.setBackgroundResource(R.drawable.circle_black_border_pressed);
        openDoor.setBackgroundResource(R.drawable.circle_black_border);
    }

    public void openDoorPressed() {
        ImageButton openDoor = findViewById(R.id.openDoor);
        ImageButton closeDoor = findViewById(R.id.closeDoor);

        openDoor.setBackgroundResource(R.drawable.circle_black_border_pressed);
        closeDoor.setBackgroundResource(R.drawable.circle_black_border);
    }

    public void onSettingsClick(View view) {
        DialogFragment settingsDialog = new SettingsDialog();
        settingsDialog.show(getFragmentManager(), "Settings");
    }

    /**
     * Check if NFC is enabled every second
     */
    public void runNfcCheckInBackground() {
        final Timer checkImage = new Timer();
        final int delay = 1000; // 1 second
        checkImage.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                checkNFC();
            }
        }, 0, delay);
    }

    public void checkNFC() {
        runOnUiThread(() -> {
            ImageButton NFC = findViewById(R.id.nfc);
            try {
                NfcManager manager = (NfcManager) getApplicationContext().getSystemService(Context.NFC_SERVICE);
                NfcAdapter adapter = null;
                if (manager != null)
                    adapter = manager.getDefaultAdapter();
                if (adapter != null && adapter.isEnabled()) {
                    NFC.setBackgroundResource(R.drawable.circle_green_border);
                } else {
                    NFC.setBackgroundResource(R.drawable.circle_red_border);
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        });
    }

    public void startStream() {
        // Start stream async task
        try {
            Stream stream = new Stream();
            stream.execute().get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    private class Stream extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {
            WebSocketFactory factory = new WebSocketFactory();

            // Create a WebSocket. The timeout value set above is used.
            try {
                ws = factory.createSocket(STREAM_IP);

                ws.addListener(new WebSocketAdapter() {
                    @Override
                    public void onConnected(WebSocket websocket, Map<String, List<String>> headers) {
                        ws.sendText(CONNECT_MESSAGE);
                    }

                    @Override
                    public void onTextMessage(WebSocket websocket, String message) {
                        updateStreamFrame(message);
                        ws.sendText(CONNECT_MESSAGE);
                    }

                    @Override
                    public void onConnectError(WebSocket websocket, WebSocketException exception) {
                        ws.connectAsynchronously();
                    }
                });

                ws.connectAsynchronously();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return Boolean.TRUE;
        }
    }

    private void updateStreamFrame(final String txt) {
        runOnUiThread(() -> {
            decodedString = Base64.decode(txt, Base64.DEFAULT);
            decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            streamImageView.setImageBitmap(decodedByte);
            decodedByte.recycle();
        });
    }

    private void stopConnection() {
        if (ws != null) {
            ws.disconnect();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopConnection();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopConnection();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopConnection();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startStream();
    }

    public void scaleButtons() {
        final ImageButton openDoor = findViewById(R.id.openDoor);
        final ImageButton closeDoor = findViewById(R.id.closeDoor);
        final ImageButton settings = findViewById(R.id.settings);
        final CardView userName = findViewById(R.id.cardView);

        openDoor.post(() -> {
            //Both the OpenDoor and the CloseDoor button have the same size
            int lockButtonDiameter = openDoor.getMeasuredWidth();

            //Settings Button diameter
            int settingsButtonDiameter = settings.getMeasuredWidth();

            // Padding = 25% of button diameter
            int lockButtonPadding = (int) Math.round(lockButtonDiameter * 0.25);

            // Padding = 10% of button diameter
            int settingsButtonPadding = (int) Math.round(settingsButtonDiameter * 0.1);

            // 50% of CardView Width
            float cardviewAdjustedCornerRadius = Math.round(userName.getMeasuredWidth() * 0.1);

            openDoor.setPadding(lockButtonPadding, lockButtonPadding, lockButtonPadding, lockButtonPadding);
            closeDoor.setPadding(lockButtonPadding, lockButtonPadding, lockButtonPadding, lockButtonPadding);
            settings.setPadding(settingsButtonPadding, settingsButtonPadding, settingsButtonPadding, settingsButtonPadding);
            userName.setRadius(cardviewAdjustedCornerRadius);
        });
    }


    public void showMessage(String message) {
        snackbar = Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG);
        snackbar.setAction("DISMISS", v -> snackbar.dismiss());
        snackbar.setActionTextColor(Color.rgb(66, 149, 154));
        snackbar.show();
    }
}
