package com.example.kyrill.cda;

import android.nfc.cardemulation.HostApduService;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.util.Arrays;

//HCE
public class HostCardEmulatorService extends HostApduService {

    private static final byte[] APDU_SELECT_TOKEN = {
            (byte) 0x00, // CLA
            (byte) 0xA4, // INS
            (byte) 0x04, // P1
            (byte) 0x00, // P2
            (byte) 0x07, // Lc field
            (byte) 0xF0, (byte) 0x39, // NDEF Tag Application name
            (byte) 0x41, (byte) 0x48, // NDEF Tag Application name
            (byte) 0x14, (byte) 0x81, // NDEF Tag Application name
            (byte) 0x00, // NDEF Tag Application name
            (byte) 0x00  // Le field
    };

    private static final byte[] APDU_SELECT_DOOR = {
            (byte) 0x00, // CLA
            (byte) 0xA4, // INS
            (byte) 0x04, // P1
            (byte) 0x00, // P2
            (byte) 0x07, // Lc field
            (byte) 0xF0, (byte) 0x01, // NDEF Tag Application name
            (byte) 0x02, (byte) 0x03, // NDEF Tag Application name
            (byte) 0x04, (byte) 0x05, // NDEF Tag Application name
            (byte) 0x06, // NDEF Tag Application name
            (byte) 0x00  // Le field
    };


    @Override
    public byte[] processCommandApdu(byte[] commandApdu, Bundle extras) {
        byte[] responseApdu = new byte[]{(byte) 0x6F, (byte) 0x00};
        Toast.makeText(getApplicationContext(), Arrays.toString(getApdu(commandApdu)), Toast.LENGTH_LONG).show();
        Log.w("APDU:", Arrays.toString(getApdu(commandApdu)));
        // TOKEN AUTHENTICATOR
        if (Arrays.equals(getApdu(commandApdu), APDU_SELECT_TOKEN)) {
            byte[] token = getToken(commandApdu);

            return tokenAuth(token);
            // DOOR AUTHENTICATION
        } else if (Arrays.equals(getApdu(commandApdu), APDU_SELECT_DOOR)) {
            return doorAuth();
        }

        return responseApdu;
    }

    // TODO
    public byte[] doorAuth() {
        byte[] responseApdu;

        // Send Token to Raspberry Pi
        String Token = UserSession.getToken(getApplicationContext());
        if(Token.isEmpty())
        {
            Toast.makeText(getApplicationContext(), "No Token registered", Toast.LENGTH_LONG).show();
        }

        byte[] id = hexStringToByteArray(Token);

        Log.w("TOKEN:", Token);

        responseApdu = new byte[id.length + 3];
        // Copies wanted token to responseApdu
        System.arraycopy(id, 0, responseApdu, 0, id.length);

        // Adding 9000 Success to end of response
        int state = UserSession.getDoorState(this);

        if (state == 0)
        {
            Toast.makeText(getApplicationContext(), "Closing Door!", Toast.LENGTH_LONG).show();
        }else {
            Toast.makeText(getApplicationContext(), "Opening Door!", Toast.LENGTH_LONG).show();
        }
        responseApdu[id.length] = (byte) state;

        Log.w("response:", Arrays.toString(responseApdu));
        return responseApdu;
    }

    public byte[] tokenAuth(byte[] token) {
        byte[] successApdu = new byte[]{(byte) 0x90, (byte) 0x00};
        String hexToken = ByteArrayToHexString(token);
        UserSession.setToken(getApplicationContext(), hexToken);
        Toast.makeText(getApplicationContext(),"Token Authenticated!\n"+hexToken,
                Toast.LENGTH_LONG).show();
        return successApdu;
    }

    @Override
    public void onDeactivated(int reason) {
        // Occurs if connection to reader is lost or new select APDU is called
    }

    public String ByteArrayToHexString(byte[] bytes) {
        final char[] hexArray = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        char[] hexChars = new char[bytes.length * 2]; // Each byte has two hex characters (nibbles)
        int v;
        for (int j = 0; j < bytes.length; j++) {
            v = bytes[j] & 0xFF; // Cast bytes[j] to int, treating as unsigned value
            hexChars[j * 2] = hexArray[v >>> 4]; // Select hex character from upper nibble
            hexChars[j * 2 + 1] = hexArray[v & 0x0F]; // Select hex character from lower nibble
        }
        return new String(hexChars);
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    public byte[] getToken(byte[] apdu) {
        // Select Message + AID { 0x00, 0xA4, 0x04, 0x00, 0x07, 0xF0, 0x39, 0x41, 0x48, 0x14, 0x81, 0x00, 0x00,
        // Token 0x39, 0x53, 0x43, 0x07};
        return Arrays.copyOfRange(apdu, 13, apdu.length);
    }

    public byte[] getApdu(byte[] apdu) {
        return Arrays.copyOfRange(apdu, 0, 13);
    }
}
