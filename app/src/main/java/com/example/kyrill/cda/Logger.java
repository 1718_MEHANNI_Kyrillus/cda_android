package com.example.kyrill.cda;

import android.content.Context;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public final class Logger {

    private static final String LOG_FILE_NAME = "logs";

    // DARK BLUE | RED | DARKISH YELLOW
    enum messageType {
        INFO, ERROR, WARNING
    }


    public static void writeEntryToLogs(Context context, String message, String responsecode, messageType type) {
        FileOutputStream outputStream;

        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MMM.yyyy - HH:mm:ss", Locale.GERMAN);
        String formattedDate = dateFormatter.format(date);

        try {
            outputStream = new FileOutputStream(createFileIfNotExists(context), true);
            outputStream.write((formattedDate + "\n" + type.toString() + "!\nMessage: " + message
                    + ((responsecode != null && !responsecode.isEmpty()) ? ("\nResponse Code: " + responsecode) : "")
                    + "\n\n").getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String readEntriesFromLogs(Context context) {
        File logs = createFileIfNotExists(context);

        StringBuilder text = new StringBuilder();

        String line;

        try {
            BufferedReader br = new BufferedReader(new FileReader(logs));

            while ((line = br.readLine()) != null) {
                text.append(line.replace("ERROR!", redText("ERROR!"))
                        .replace("INFO!", darkBlueText("INFO!"))
                        .replace("WARNING!", darkishYellowText("WARNING!")));
                text.append("<br>");
            }
            br.close();


        } catch (IOException e) {
            e.printStackTrace();
        }

        return text.toString();
    }

    private static File createFileIfNotExists(Context context) {
        // file:///storage/emulated/0/Android/data/com.example.kyrill.cda/files/logs
        try {
            File path = context.getExternalFilesDir(null);
            File log_file = new File(path, LOG_FILE_NAME);
            // does nothing if File already exists
            log_file.createNewFile();

            return log_file;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static String redText(String message) {
        return  "<font color='#db0618''>" + message + "</font>";
    }

    private static String darkBlueText(String message) {
        return  "<font color=\"#1a407c\">" + message + "</font>";
    }

    private static String darkishYellowText(String message) {
        return  "<font color=\"#cddb13\">" + message + "</font>";
    }
}
