package com.example.kyrill.cda;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

import static com.example.kyrill.cda.Logger.readEntriesFromLogs;


public class LogsActivity extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logs);

        TextView logs = findViewById(R.id.text_logs);
        logs.setMovementMethod(new ScrollingMovementMethod());
        logs.setText(Html.fromHtml(readEntriesFromLogs(this)));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        leaveActivityAnimation();
    }

    protected void leaveActivityAnimation() {
        this.overridePendingTransition(R.transition.enter_from_left, R.transition.exit_to_right);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        leaveActivityAnimation();
    }

    @Override
    protected void onStop() {
        super.onStop();
        leaveActivityAnimation();
    }

    @Override
    protected void onPause() {
        super.onPause();
        leaveActivityAnimation();
    }
}
