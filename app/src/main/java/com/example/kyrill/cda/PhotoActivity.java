package com.example.kyrill.cda;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.support.design.widget.Snackbar;
import android.support.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;


import java.io.ByteArrayOutputStream;
import java.io.File;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import static com.example.kyrill.cda.Logger.writeEntryToLogs;
import static com.example.kyrill.cda.R.string.*;
import static com.example.kyrill.cda.ServerUtils.ERROR_MESSAGE;
import static com.example.kyrill.cda.ServerUtils.OAUTH_TOKEN;
import static com.example.kyrill.cda.ServerUtils.OK_RESPONSE_CODE;
import static com.example.kyrill.cda.ServerUtils.RESPONSE_CODE;
import static com.example.kyrill.cda.ServerUtils.RESPONSE_MESSAGE;
import static com.example.kyrill.cda.ServerUtils.RESPONSE_OAUTH;
import static com.example.kyrill.cda.ServerUtils.SEND_PHOTO;

public class PhotoActivity extends AppCompatActivity {

    static final int REQUEST_IMAGE_CAPTURE = 1;
    ImageView photo;
    String imageFilePath;
    private Snackbar snackbar;
    private Bitmap bitmap;
    private Uri imageURI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);
        scaleButtons();
        if (savedInstanceState == null) {
            showCamera();
        }
    }

    private void showCamera() {
        TextView textView = findViewById(R.id.textView4);

        if (checkCameraHardware()) {
            dispatchTakePictureIntent();
            textView.setVisibility(View.GONE);

        } else {
            textView.setVisibility(View.VISIBLE);
            textView.setText(no_camera_available);
        }
    }

    private boolean checkCameraHardware() {
        return this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.kyrill.cda",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    private File createImageFile() throws IOException {
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir =
                getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,    /* prefix */
                ".jpg",    /* suffix */
                storageDir      /* directory */
        );

        imageFilePath = image.getAbsolutePath();
        return image;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            photo = findViewById(R.id.photo);
            File imgFile = new File(imageFilePath);

            imageURI = Uri.fromFile(new File(imgFile.getAbsolutePath()));
            try {
                bitmap = handleSamplingAndRotationBitmap(this, imageURI);
                if (imgFile.exists()) {
                    photo.setImageBitmap(bitmap);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            Intent intent = new Intent(PhotoActivity.this, HomeActivity.class);
            startActivity(intent);
            finish();
            leaveActivityAnimation();
        }
    }

    public void scaleButtons() {
        final ImageButton save = findViewById(R.id.save);
        final ImageButton redoPhoto = findViewById(R.id.redoPhoto);

        save.post(() -> {
            //Both the OpenDoor and the CloseDoor button have the same size
            int saveMeasuredWidth = save.getMeasuredWidth();

            // Padding = 20% of button diameter
            int saveButtonPadding = (int) Math.round(saveMeasuredWidth * 0.20);

            save.setPadding(saveButtonPadding, saveButtonPadding, saveButtonPadding, saveButtonPadding);
            redoPhoto.setPadding(saveButtonPadding, saveButtonPadding, saveButtonPadding, saveButtonPadding);
        });
    }

    public static Bitmap handleSamplingAndRotationBitmap(Context context, Uri selectedImage) throws IOException {
        int MAX_HEIGHT = 1024;
        int MAX_WIDTH = 1024;

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        InputStream imageStream = context.getContentResolver().openInputStream(selectedImage);
        BitmapFactory.decodeStream(imageStream, null, options);
        if (imageStream != null) {
            imageStream.close();
        }

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, MAX_WIDTH, MAX_HEIGHT);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        imageStream = context.getContentResolver().openInputStream(selectedImage);
        Bitmap img = BitmapFactory.decodeStream(imageStream, null, options);

        img = rotateImageIfRequired(context, img, selectedImage);
        return img;
    }

    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee a final image
            // with both dimensions larger than or equal to the requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;

            // This offers some additional logic in case the image has a strange
            // aspect ratio. For example, a panorama may have a much larger
            // width than height. In these cases the total pixels might still
            // end up being too large to fit comfortably in memory, so we should
            // be more aggressive with sample down the image (=larger inSampleSize).

            final float totalPixels = width * height;

            // Anything more than 2x the requested pixels we'll sample down further
            final float totalReqPixelsCap = reqWidth * reqHeight * 2;

            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++;
            }
        }
        return inSampleSize;
    }

    private static Bitmap rotateImageIfRequired(Context context, Bitmap img, Uri selectedImage) throws IOException {

        InputStream input = context.getContentResolver().openInputStream(selectedImage);
        ExifInterface ei;
        if (Build.VERSION.SDK_INT > 23) {
            assert input != null;
            ei = new ExifInterface(input);
        } else
            ei = new ExifInterface(selectedImage.getPath());

        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    public void onRedoPhotoClick(View view) {
        showCamera();
    }

    public void onSaveImageClick(View view) {
        try {
            ServerConnection serverConnection = new ServerConnection(getApplicationContext());

            String base64 = imageToBase64();

            String token = UserSession.getToken(this);

            List<String> response_oauth = serverConnection.execute(OAUTH_TOKEN).get();

            String responseMessage = ERROR_MESSAGE;

            if (response_oauth.get(RESPONSE_CODE).equals(OK_RESPONSE_CODE))
            {
                String access_token = response_oauth.get(RESPONSE_OAUTH);

                List<String> response_request = serverConnection.execute(SEND_PHOTO, token, base64, access_token).get();

                responseMessage = response_request.get(RESPONSE_MESSAGE);
            }

            showMessage(responseMessage);

        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            showMessage(ERROR_MESSAGE);
            writeEntryToLogs(this, e.getMessage(), null, Logger.messageType.ERROR);
        }
    }


    public String imageToBase64() {

        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap = handleSamplingAndRotationBitmap(this, imageURI);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            return Base64.encodeToString(byteArray, Base64.NO_WRAP);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public void showMessage(String message) {
        snackbar = Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG);
        snackbar.setAction("DISMISS", v -> snackbar.dismiss());
        snackbar.setActionTextColor(Color.rgb(66, 149, 154));
        snackbar.show();
    }


    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        if (imageURI != null) {
            savedInstanceState.putParcelable("imageURI", imageURI);
        }
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState != null) {
            try {
                imageURI = savedInstanceState.getParcelable("imageURI");
                photo = findViewById(R.id.photo);
                photo.setImageBitmap(handleSamplingAndRotationBitmap(this, imageURI));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    protected void leaveActivityAnimation() {
        overridePendingTransition(R.transition.enter_from_left, R.transition.exit_to_right);
    }
}
