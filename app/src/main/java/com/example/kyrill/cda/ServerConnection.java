package com.example.kyrill.cda;

import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static com.example.kyrill.cda.Logger.writeEntryToLogs;
import static com.example.kyrill.cda.ServerUtils.ACCESS_TOKEN;
import static com.example.kyrill.cda.ServerUtils.ERROR_MESSAGE;
import static com.example.kyrill.cda.ServerUtils.ERROR_RESPONSE_CODE;
import static com.example.kyrill.cda.ServerUtils.OAUTH_TOKEN;
import static com.example.kyrill.cda.ServerUtils.OK_RESPONSE_CODE;
import static com.example.kyrill.cda.ServerUtils.PHOTO;
import static com.example.kyrill.cda.ServerUtils.PROTOCOL;
import static com.example.kyrill.cda.ServerUtils.SEND_PHOTO;
import static com.example.kyrill.cda.ServerUtils.SERVER_IP;
import static com.example.kyrill.cda.ServerUtils.TOKEN;


/**
 * Returns List with two Strings
 * First String [0] = ResponseMessage
 * Second String [1] = ResponseCode
 * Additionally the result will be logged on the phone!
 **/

public class ServerConnection extends AsyncTask<String, String, List<String>> {

    private static HttpURLConnection HTTP_CONNECTION;

    private final WeakReference<Context> context;

    public ServerConnection(Context context) {
        this.context = new WeakReference<>(context);
    }

    @Override
    protected List<String> doInBackground(String... strings) {

        //Get Context
        Context ctx = context.get();

        List<String> responses = new ArrayList<>();

        try {
            //Given Request
            String REQUEST = strings[0];

            //Establish Connection
            establishConnection(REQUEST);

            switch (REQUEST) {
                case SEND_PHOTO:
                    sendPhoto(strings[PHOTO], strings[TOKEN], strings[ACCESS_TOKEN]);
                    break;
                case OAUTH_TOKEN:
                    getOAuthToken();
                    break;
            }

            responses = getResponses(ctx, REQUEST);

            return responses;

        } catch (Exception e) {
            writeEntryToLogs(ctx, e.getMessage(), null, Logger.messageType.ERROR);
        } finally {
            closeConnection();
        }

        //default error
        responses.add(ERROR_MESSAGE);
        responses.add(ERROR_RESPONSE_CODE);

        return responses;
    }

    private List<String> getResponses(Context ctx, String REQUEST) throws IOException {

        List<String> responses = new ArrayList<>();

        BufferedReader br;
        if (HTTP_CONNECTION.getResponseCode() == Integer.valueOf(OK_RESPONSE_CODE)) {
            br = new BufferedReader(new InputStreamReader(HTTP_CONNECTION.getInputStream()));
        } else {
            br = new BufferedReader(new InputStreamReader(HTTP_CONNECTION.getErrorStream()));
        }

        String responseMessage = br.readLine();

        String responseCode = Integer.toString(HTTP_CONNECTION.getResponseCode());

        writeEntryToLogs(ctx, responseMessage, responseCode, Logger.messageType.INFO);

        responses.add(responseMessage);
        responses.add(responseCode);

        if (REQUEST.equals(OAUTH_TOKEN)) {
            responses.add(HTTP_CONNECTION.getRequestProperty("accessToken"));
        }

        return responses;
    }


    private void establishConnection(String REQUEST) throws IOException {
        URL URL_SERVER = new URL(PROTOCOL + "://" + SERVER_IP.concat(REQUEST));
        HTTP_CONNECTION = (HttpURLConnection) URL_SERVER.openConnection();
        HTTP_CONNECTION.setConnectTimeout(5000);
    }

    private void closeConnection() {
        if (HTTP_CONNECTION != null)
            HTTP_CONNECTION.disconnect();
    }


    private void getOAuthToken() throws IOException {
        // Encoding and Format
        String clientId = "android";
        String clientSecret = "android";
        String auth = clientId + ":" + clientSecret;
        String authentication = "";

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            authentication =  java.util.Base64.getEncoder().encodeToString(auth.getBytes());
        }

        HTTP_CONNECTION.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        HTTP_CONNECTION.setRequestProperty("Authorization", "Basic "+ authentication);
        HTTP_CONNECTION.setRequestProperty("Accept", "application/json");
        HTTP_CONNECTION.setRequestMethod("POST");
        HTTP_CONNECTION.setDoInput(true);
        HTTP_CONNECTION.setDoOutput(true);

        OutputStreamWriter writer = new OutputStreamWriter(HTTP_CONNECTION.getOutputStream());
        writer.close();
    }

    private void sendPhoto(String token, String base64, String access_token) throws JSONException, IOException {
        // Encoding and Format
        HTTP_CONNECTION.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        HTTP_CONNECTION.setRequestMethod("POST");
        HTTP_CONNECTION.setRequestProperty("access_token", access_token);
        HTTP_CONNECTION.setDoInput(true);
        HTTP_CONNECTION.setDoOutput(true);

        JSONObject credentials = new JSONObject();
        credentials.put("image", base64);
        credentials.put("token", token);

        OutputStreamWriter writer = new OutputStreamWriter(HTTP_CONNECTION.getOutputStream());
        writer.write(credentials.toString());
        writer.close();
    }
}
