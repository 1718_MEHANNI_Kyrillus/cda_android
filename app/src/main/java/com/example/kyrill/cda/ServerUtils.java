package com.example.kyrill.cda;

public final class ServerUtils {
    public static final String SEND_PHOTO = "/api/image-app";
    public static final String OAUTH_TOKEN = "/oauth/token?grant_type=client_credentials&scope=photo";
    public static final String OK_RESPONSE_CODE = "200";
    public static final String ERROR_RESPONSE_CODE = "400";
    public static final String SERVER_IP = BuildConfig.SERVER_IP;
    public static final String STREAM_IP = BuildConfig.STREAM_IP;
    public static final String PROTOCOL = BuildConfig.PROTOCOL;
    public static final String ERROR_MESSAGE = "An error occured!";
    public static final Integer PHOTO = 1;
    public static final Integer TOKEN = 2;
    public static final Integer ACCESS_TOKEN = 3;
    public static final Integer RESPONSE_MESSAGE = 0;
    public static final Integer RESPONSE_CODE = 1;
    public static final Integer RESPONSE_OAUTH = 3;
    public static final Integer BUTTON_STATE_CLOSE = 0;
    public static final Integer BUTTON_STATE_OPEN = 1;
}
