package com.example.kyrill.cda;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;

public class SettingsDialog extends DialogFragment {

    private static final int TAKE_PHOTO = 0;
    private static final int SHOW_LOGS = 1;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Settings")
                .setCancelable(true)
                .setIcon(R.drawable.ic_settings)
                .setNegativeButton("Cancel", (dialog, selection) -> dismiss())
                .setItems(R.array.settings, (dialog, selection) -> {

                    if (selection == SHOW_LOGS) {
                        Intent intent = new Intent(getActivity(), LogsActivity.class);
                        startActivity(intent);
                        leaveActivityAnimation();

                    } else if (selection == TAKE_PHOTO) {
                        Intent intent = new Intent(getActivity(), PhotoActivity.class);
                        startActivity(intent);
                        leaveActivityAnimation();

                    }
                });
        return builder.create();
    }

    protected void leaveActivityAnimation() {
        getActivity().overridePendingTransition(R.transition.enter_from_left, R.transition.exit_to_right);
    }
}