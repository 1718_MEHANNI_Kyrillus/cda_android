package com.example.kyrill.cda;

import android.content.Context;
import android.content.SharedPreferences;

public class UserSession {

    private static final String USERNAME = "Username";
    private static final String SHARED_FILE_NAME = "USER";
    private static final String TOKEN = "Token";
    private static final String DOOR_STATE = "DoorState";

    static SharedPreferences getSharedPreferences(Context ctx) {
        return ctx.getSharedPreferences(SHARED_FILE_NAME, Context.MODE_PRIVATE);
    }

    public static void setUserName(Context ctx, String userName) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(USERNAME, userName);
        editor.apply();
    }

    public static String getUserName(Context ctx) {
        return getSharedPreferences(ctx).getString(USERNAME, "");
    }

    public static void removeUserName(Context ctx)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.remove(USERNAME);
        editor.apply();
    }

    public static void setDoorState(Context ctx, int state)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putInt(DOOR_STATE, state);
        editor.apply();
    }

    public static int getDoorState(Context ctx)
    {
        return getSharedPreferences(ctx).getInt(DOOR_STATE, 1);
    }

    public static void setToken(Context ctx, String token)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(TOKEN, token);
        editor.apply();
    }

    public static String getToken(Context ctx) {
        return getSharedPreferences(ctx).getString(TOKEN, "");
    }

}
